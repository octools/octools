var boutonEnvoi = document.getElementById("btEnvoie");
boutonEnvoi.disabled = false;
var form = document.querySelector("form");
var iconAttente = document.getElementById("iconAttente"); // icon animée apparaissant dans le bouton lors de l'envoie du formulaire
var alerteActive = false; // Défini si le message d'alerte est déjà affiché
var regexMail = /^[a-z0-9\.\-_]+@[a-z0-9\.\-_]{2,}\.[a-z]{2,4}$/;

// Envoie du formulaire
form.addEventListener("submit", function (e) {
    e.preventDefault();
    var demandeInvitation = {
        email: form.elements.email.value,
        type: "member"
    };
    ajaxPost("https://ocetudients.ryver.com/api/1/odata.svc/User.Invite()?$format=json", demandeInvitation, function () {
        // Affichage d'un message confirmant l'envoie
        var message = document.createElement("p");
        message.textContent = "L'invitation a été envoyée. Merci de consulter ta boite mail.";
        message.classList = "bg-success";
        document.getElementById("messageInfo").appendChild(message);
        boutonEnvoi.disabled = "disabled"; // désactive le bouton d'envoie pour éviter les soumissions multiples
        iconAttente.style.display = "none"; // Le traitement de l'envoie étant terminé l'icon d'attente disparait
    }, true, true);
    // la dernière valeur définie si les paramêtres d'autorisation définis dans ajax.js doivent être transmis dans le header
});

// Affiche le message d'erreur si le format email n'est pas correcte et que le message n'est pas déjà affiché
document.getElementById("email").addEventListener("blur", function (e) {
    if (!regexMail.test(e.target.value) && !alerteActive) {
        // Affichage du message signalant un problème dans le format de l'adresse email
        var messageAlerte = document.createElement("p");
        messageAlerte.textContent = "Le format de ton email est incorrect.";
        messageAlerte.classList = "text-danger";
        document.getElementById("messageAlerte").appendChild(messageAlerte);
        // Signale le champ comme présentant un problème et bloque le bouton d'envoie du formulaire
        var champEmail = document.getElementById("champEmail");
        champEmail.classList.add("has-error");
        boutonEnvoi.disabled = "disabled";
        alerteActive = true;
        verifEmail();
    }
});

// Si un problème a été détecté avec l'adresse mail saisie, alors les modifications de saisie sont testées en live afin de vérifier le moment où le regex est respecté afin de débloquer le bouton d'envoie
function verifEmail() {
    document.getElementById("email").addEventListener("input", function (e) {
        if (regexMail.test(e.target.value)) {
            document.getElementById("messageAlerte").innerHTML = "";
            var champEmail = document.getElementById("champEmail");
            champEmail.classList.remove("has-error");
            boutonEnvoi.disabled = false; // Le bouton est déplaqué
            alerteActive = false; // Le problème est réglé
        };
    });
};

// Affiche l'icon annimé dans le bouton pour signaler le traitement de l'action demandée
document.getElementById("btEnvoie").addEventListener("click", function (e) {
    iconAttente.style.display = "inline-block";
});
